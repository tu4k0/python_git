def parse(query: str) -> dict:
    result = {}
    queries = query.split('?')[-1].split('&')
    for query_pair in queries:
        if '=' in query_pair:
            key, value = query_pair.split('=')
            result[key] = value
    return result

if __name__ == '__main__':
    assert parse('https://example.com/path/to/page?name=ferret&color=purple') == {'name': 'ferret', 'color': 'purple'}
    assert parse('https://example.com/path/to/page?name=ferret&color=purple&') == {'name': 'ferret', 'color': 'purple'}
    assert parse('http://example.com/') == {}
    assert parse('http://example.com/?') == {}
    assert parse('http://example.com/?name=Dima') == {'name': 'Dima'}

