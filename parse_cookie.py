def parse_cookie(query: str) -> dict:
    result = {}
    queries = query.split(';')
    for query_pair in queries:
        if '=' in query_pair:
            key, value = query_pair.split('=', maxsplit=1)
            result[key] = value
        if '' in query_pair:
            del query_pair
    return result

if __name__ == '__main__':
    assert parse_cookie('name=Dima;') == {'name': 'Dima'}
    assert parse_cookie('') == {}
    assert parse_cookie('name=Dima;age=28;') == {'name': 'Dima', 'age': '28'}
    assert parse_cookie('name=Dima=User;age=28;') == {'name': 'Dima=User', 'age': '28'}
